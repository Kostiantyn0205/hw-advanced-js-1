/*1) Якщо спробувати звернутись до властивостей або методів об'єкта, які не знаходяться в ньому, JavaScript буде шукати ці властивості у прототипі.
Якщо властивість не знайдено у самому об'єкті або його прототипі, пошук буде продовжено вгору по ланцюжку прототипів до тих пір, поки властивість
не буде знайдено або дійдено до останнього прототипу, який є null.*/

/*2) Виклик super() дозволяє передати аргументи до конструктора батьківського класу, щоб правильно ініціалізувати
його властивості, які будуть використані у класі-нащадку.*/

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(newName) {
        this._name = newName;
    }

    get age() {
        return this._age;
    }

    set age(newAge) {
        this._age = newAge;
    }

    get salary() {
        return this._salary;
    }

    set salary(newSalary) {
        this._salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get lang() {
        return this._lang;
    }

    set lang(newLang) {
        this._lang = newLang;
    }

    get salary() {
        return super.salary * 3;
    }

    set salary(newSalary) {
        super.salary = newSalary;
    }
}

const employee = new Programmer("John", 30, 5000, ["Java", "C++"]);

console.log(employee.name);
console.log(employee.age);
console.log(employee.salary);
console.log(employee.lang);

employee.salary = 6000;
console.log(employee.salary);

const programmer = new Programmer("Alice", 25, 4000, ["JavaScript", "Python"]);

console.log(programmer.name);
console.log(programmer.age);
console.log(programmer.salary);
console.log(programmer.lang);